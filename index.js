const express = require('express')
const mongoose = require('mongoose')
const bodyParser = require('body-parser')

const app = express()
const port = 3000
const mongoConfig = { useNewUrlParser: true }

mongoose.connect('mongodb://localhost:27017/test', mongoConfig);

app.use(bodyParser.urlencoded({ extended: true }))
app.use(bodyParser.json())

const rutas = require('./pelis')
app.use('/', rutas)

app.listen(port, () => console.log(`Example app listen on port ${port}!`))
