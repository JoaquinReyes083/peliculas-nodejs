const mongoose = require('mongoose')

var schema = new mongoose.Schema({
    titulo: { 
        type: String,
        required: true,
    },
    director:{
        type: String,
        required: true,
    },
    año: {
        type: Number,
        required: true,
    },
    estaChida: {
        type: Boolean,
        required: true,
    }
})

var model = mongoose.model('Movies', schema)

module.exports = model