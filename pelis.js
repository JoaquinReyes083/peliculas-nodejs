const router = require('express').Router()
const Movies = require('./models/users')

router.post('/peliculas', (req, res) => {
    //req.body muestra los parametros que le mandas
    console.log(req.body)
    const peliculaNuevo = {
        titulo: req.body.titulo,
        director: req.body.director,
        año: req.body.año,
        estaChida: req.body.estaChida,
    }
    return new Movies(peliculaNuevo)
        .save()
        .then((doc) => {
            console.log('Pelicula guardada en la base de datos', doc)
            res.json(doc)
        })
        .catch((error) => {
            const message = 'Error al guardar pelicula en la DB'
            console.error(message, error)
            res.status(500).json({ message })
        })

    res.send('Creando la pelicula, aguarde un momento')
})

router.put('/peliculas/:id/:actu', (req, res) => {
    const id = req.params.id
    Movies.findByIdAndUpdate(id, { estaChida: req.params.actu })
        .then((doc) => {
            res.send('Pelicula con el ID ' + id + ' ha sido actualizada')
            console.log('Pelicula con el ID ' + id + 'ha sido actualizada')
        })
        .catch((error) => {
            const message = 'Error al actualizar pelicula en la DB'
            console.error(message, error)
        })
})

router.delete('/peliculas/:titulo', (req, res) => {
    const del = req.params.titulo
    Movies.findOneAndRemove(del)
        .then((doc) => {
            console.log('EL pelicula ' + del + ' ha sido eliminado')
            res.send('El pelicula ' + del + ' ha sido eliminado')
        })
        .catch((error) => {
            const message = 'Error al borrar pelicula en la DB'
            console.error(message, error)
        })
})

router.delete('/peliculas/:id', (req, res) => {
    const del = req.params.id
    Movies.findOneAndRemove(del)
        .then((doc) => {
            console.log('La pelicula ' + del + ' ha sido eliminada')
            res.send('La pelicula ' + del + ' ha sido eliminada')
        })
        .catch((error) => {
            const message = 'Error al borrar pelicula en la DB'
            console.error(message, error)
        })
})

router.get('/peliculas', (req, res) => {
    Movies.find({})
        .then((data) => {
            console.log(data);
            res.json(data)
        })
        .catch((error) => {
            const message = 'Error al obtener pelicula en la DB'
            console.error(message, error)
        })
})

router.get('/peliculas/:titulo', (req, res) => {
    const titulo = req.params.titulo
    Movies.find({ titulo })
        .then((doc) => {
            console.log(doc);
            res.json(doc)
        })
        .catch((error) => {
            const message = 'Error al obtener pelicula en la DB'
            console.error(message, error)
        })
})

module.exports = router